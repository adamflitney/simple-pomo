import React, { Component } from 'react';
import { CircularProgressbar } from 'react-circular-progressbar';
import ReactHowler from 'react-howler';
import PomoSettings from './PomoSettings';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog } from '@fortawesome/free-solid-svg-icons'
import 'react-circular-progressbar/dist/styles.css';
import './Pomo.css';

class Pomo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            workTimeSeconds: 25 * 60,
            breakTimeSeconds: 5 * 60,
            audioEnabled: true,
            notificationsEnabled: false,
            timeRemainingSeconds: 25 * 60,
            running: false,
            timePhase: 'work',
            timer: null,
            playSound: false,
            startTime: null,
            showSettings: false
        };

        
    }


    componentDidMount() {
        this.setState({
            timer: setInterval(this.tick, 1000)
        });

        // if("Notification" in window) {
        //     console.log(Notification.permission);
        //     if(Notification.permission !== "granted") {
        //         Notification.requestPermission();
        //     }
        // }
    }

    componentWillUnmount() {
        clearInterval(this.state.timer);
        this.setState({
            timer: null
        });

        
    }

    tick = () =>  {
        if(this.state.running) {
            if (this.state.timeRemainingSeconds > 0) {
                let timerLength = this.state.timePhase === "work" ? this.state.workTimeSeconds : this.state.breakTimeSeconds;
                let timerExpiry = this.state.startTime + (timerLength * 1000);
                this.setState({
                    timeRemainingSeconds: Math.floor((timerExpiry - Date.now()) / 1000) 
                });
                
            } else {
                if (this.state.timePhase === "work") {
                    this.setState({
                        timePhase: 'break',
                        timeRemainingSeconds: this.state.breakTimeSeconds,
                        running: false,
                        playSound: this.state.audioEnabled
                    });
                    if(this.state.notificationsEnabled) {
                        let notification = new Notification("Take a break.");
                    }
                    
                } else  if (this.state.timePhase === 'break') {
                    this.setState({
                        timePhase: 'work',
                        timeRemainingSeconds: this.state.workTimeSeconds,
                        running: false,
                        playSound: this.state.audioEnabled
                    });
                    if(this.state.notificationsEnabled) {
                        let notification = new Notification("Back to work!");
                    }
                    
                }
            }

        }
    }

    reset = () => {
        console.log('reset');
        this.setState({
            timePhase: 'work',
            timeRemainingSeconds: this.state.workTimeSeconds,
            running: false,
            playSound: false,
            startTime: null
        });
    }

    updateSettings = ({audio, notify, workTimeMinutes, breakTimeMinutes}) => {
        console.log('update settings');
        const prevWorkTime = this.state.workTimeSeconds;
        const prevBreakTime = this.state.breakTimeSeconds;
        console.log(`previous work time: ${prevWorkTime} previous break time: ${prevBreakTime}`);
        this.setState({
            audioEnabled: audio,
            notificationsEnabled: notify,
            workTimeSeconds: workTimeMinutes * 60,
            breakTimeSeconds: breakTimeMinutes * 60,
            showSettings: false
        }, () => {
            console.log(`current work time: ${this.state.workTimeSeconds} current break time: ${this.state.breakTimeSeconds}`);
        if (prevWorkTime !== this.state.workTimeSeconds || prevBreakTime !== this.state.breakTimeSeconds) {
            this.reset();
        }
        });
        
    }

    toggleTimer = () => {
        this.setState({
            running: !this.state.running,
            playSound: false,
            startTime: Date.now()
        });
    }

    getProgressPercent() {
        let progressPercent;
        if( this.state.timePhase === 'work') {
            progressPercent =  this.state.timeRemainingSeconds / this.state.workTimeSeconds * 100;
        } else {
            progressPercent =  this.state.timeRemainingSeconds / this.state.breakTimeSeconds * 100;
        }
        return progressPercent;
    }

    getProgressStyles = () => {
        if (this.state.timePhase === 'break') {
            return {
                textColor: "#61dafb",
                pathColor: "#61dafb"
            }
        } else {
            return {
                textColor: "#25b449",
                pathColor: "#25b449"
            }
        }
    }

    formatTimeRemaining() {
        const minutes = Math.floor(this.state.timeRemainingSeconds / 60);
        const seconds = this.state.timeRemainingSeconds - minutes * 60;
        return `${minutes}:${seconds < 10 ? '0' + seconds : seconds}`
    }

    openSettings = () => {
        this.setState({
            showSettings: true
        });
    }

    stopSound = () => {
        this.setState({
            playSound: false
        });
    }

render() {
    return (
        <div className={`pomo ${this.state.timePhase}`}>
            <div className='settings-button' onClick={this.openSettings}>
            <FontAwesomeIcon id="settings-icon" icon={faCog} />
            </div>
            <ReactHowler
                src={this.state.timePhase === 'break' ? 'break-time.wav' : 'work-time.mp3'}
                playing={this.state.playSound}
                onEnd={this.stopSound}
            />
            {this.state.showSettings &&
                <PomoSettings onClose={this.updateSettings} currentSettings={ { workTimeSeconds: this.state.workTimeSeconds, breakTimeSeconds: this.state.breakTimeSeconds, audioEnabled: this.state.audioEnabled, notificationsEnabled: this.state.notificationsEnabled } } />
            }
            <div className="message">
                {this.state.timePhase === 'break' ? 'Time for a break!' : 'Time to work!'}
            </div> 
            <CircularProgressbar 
                value={this.getProgressPercent()} 
                text={this.formatTimeRemaining()}
                className={this.state.timePhase}
                />
            <div className="buttons">
            <div 
                className="start-stop button"
                onClick={this.toggleTimer}
            >
                {this.state.running ? 'Pause' : 'Start'}
            </div>
            <div 
            className="reset button"
            onClick={this.reset}
            >
            Reset
            </div>
            </div>
            
        </div>
        
    );
}

}

export default Pomo;