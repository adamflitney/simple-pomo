import React from 'react';
import Portal from './Portal';
import Switch from 'react-switch';
import './PomoSettings.css';


function PomoSettings(props) {
    const { onClose, currentSettings } = props
    const [values, setValues] = React.useState({
        workTimeMinutes: currentSettings.workTimeSeconds / 60,
        breakTimeMinutes: currentSettings.breakTimeSeconds / 60,
        audio: currentSettings.audioEnabled,
        notify: currentSettings.notificationsEnabled
    });

    const handleWorkTimeChange = event => {
        setValues({ ...values, workTimeMinutes: event.target.value});
    }

    const handleBreakTimeChange = event => {
        setValues({ ...values, breakTimeMinutes: event.target.value});
    }

    const handleAudioToggle = checked => {
        setValues({ ...values, audio: checked });
    };

    const handleNotificationToggle = checked => {
        setValues({ ...values, notify: checked });
        if(checked) {
            if("Notification" in window) {
                console.log(Notification.permission);
                if(Notification.permission !== "granted") {
                    Notification.requestPermission();
                }
            }
        }
    };


    return (
        <Portal id='pomo-settings'>
            <h3>Settings</h3>
            <div className='settings'>
                <span>Work Time</span>
                <input type='number' value={values.workTimeMinutes} onChange={handleWorkTimeChange} min='1'></input>
                <span>Break Time</span>
                <input type='number' value={values.breakTimeMinutes} onChange={handleBreakTimeChange} min='1'></input>
                <span>Sounds</span>
                <Switch checked={values.audio} onChange={handleAudioToggle} />
                <span>Notifications</span>
                <Switch checked={values.notify} onChange={handleNotificationToggle} />
            </div>
            <div className='close-button' onClick={() => onClose(values)}>Close</div>

        </Portal>
    );
}

export default PomoSettings;